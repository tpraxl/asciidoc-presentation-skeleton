SHELL = /bin/sh

#--------------------------------------------------------------------------
# Definitions
#--------------------------------------------------------------------------

# This way, make recognizes all .ONESHELL failures, not just the last command failure
.SHELLFLAGS := -ec

MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PWD := $(dir $(MAKEPATH))

ALL_BUILD_TARGETS := fetch-reveal-js fetch-font-awesome presentation
ALL_CLEAN_TARGETS := clean-reveal-js clean-font-awesome clean-presentation

DISTRIBUTION_DIR := public
FONT_AWESOME_PATHS_TO_BE_CLEANED := ${DISTRIBUTION_DIR}/css/font-awesome.min.css ${DISTRIBUTION_DIR}/fonts/fontawesome-webfont.* ${DISTRIBUTION_DIR}/fonts/FontAwesome.otf
REVEAL_JS_PATHS_TO_BE_CLEANED := ${DISTRIBUTION_DIR}/reveal.js
PRESENTATION_PATHS_TO_BE_CLEANED := ${DISTRIBUTION_DIR}/index.html

SERVE_PORT := 8080
SERVE_BIND := localhost


ASCIIDOCTOR_REQUIREMENT := asciidoctor-revealjs

DOCKER_PACKAGE_NAME := docker
DOCKER_EXECUTABLE_NAME := $(DOCKER_PACKAGE_NAME)

MAKE_PRESENTATION_TYPE := docker

SYSTEM_REQUIREMENTS := unzip python3 curl git

ifeq ($(MAKE_PRESENTATION_TYPE), native)
	SYSTEM_REQUIREMENTS += ${ASCIIDOCTOR_REQUIREMENT}
else
	SYSTEM_REQUIREMENTS += ${DOCKER_PACKAGE_NAME}
endif

#--------------------------------------------------------------------------
# Functions
#--------------------------------------------------------------------------

.PHONY: help all clean check-requirements 
.PHONY: $(SYSTEM_REQUIREMENTS) 
.PHONY: fetch-reveal-js clean-reveal-js update-reveal-js
.PHONY: fetch-font-awesome clean-font-awesome
.PHONY: presentation
.PHONY: serve

#: ## 
#-----------------------------: ## -----------------------------
#: ## GENERAL
#-----------------------------: ## -----------------------------

help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[\.%#a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

all: $(ALL_BUILD_TARGETS) ## Build requirements and presentation

clean: $(ALL_CLEAN_TARGETS) ## Delete generated files and directories

#: ## 
#-----------------------------: ## -----------------------------
#: ## Requirements
#-----------------------------: ## -----------------------------

$(SYSTEM_REQUIREMENTS):
	-@which "${@}" >/dev/null \
		&& echo "✓ ${@} is installed." \
		|| echo "✗ ${@} is not installed."

check-requirements: $(SYSTEM_REQUIREMENTS) ## Check for system requirements

#: ## 
#-----------------------------: ## -----------------------------
#: ## Serve
#-----------------------------: ## -----------------------------

.ONESHELL:
serve: all ## Serve the presentation
#: ## 
#: ## example: make serve SERVE_PORT=8000 
#: ## default SERVE_BIND=localhost
#: ## default SERVE_PORT=8080
	@cd ${DISTRIBUTION_DIR}
	python3 -m http.server --bind=${SERVE_BIND} ${SERVE_PORT}

#: ## 
#-----------------------------: ## -----------------------------
#: ## Presentation
#-----------------------------: ## -----------------------------

presentation: ${DISTRIBUTION_DIR}/index.html ## Build presentation from src/index.adoc

#: ## 
#: ## Native asciidoctor will be used if installed. 
#: ## Otherwise docker will be used.
#: ## 
#: ## Override docker / native mode: 
#: ## 
#: ## example: make presentation MAKE_PRESENTATION_TYPE=native
#: ## example: make presentation MAKE_PRESENTATION_TYPE=docker 

${DISTRIBUTION_DIR}/index.html: src/*.adoc
ifeq ($(MAKE_PRESENTATION_TYPE), native)
	${ASCIIDOCTOR_REQUIREMENT}  \
		-r asciidoctor-diagram \
		--source-dir ${PWD}src \
		--destination-dir ${PWD}${DISTRIBUTION_DIR} \
		${PWD}src/index.adoc
else
	${DOCKER_EXECUTABLE_NAME} run -t --rm \
		--user 1000:1000 \
		-v "${PWD}src":/documents/ \
		-v "${PWD}${DISTRIBUTION_DIR}":/public/ \
		asciidoctor/docker-asciidoctor \
		asciidoctor-revealjs  \
		-r asciidoctor-diagram \
		--source-dir /documents \
		--destination-dir /public \
		/documents/index.adoc
endif

clean-presentation: ## Delete generated presentation from public/
	-@rm ${PRESENTATION_PATHS_TO_BE_CLEANED} 2>/dev/null \
    		&& echo "Deleted ${PRESENTATION_PATHS_TO_BE_CLEANED}" \
    		|| echo "Nothing to delete for ${@}."

#: ## 
#-----------------------------: ## -----------------------------
#: ## reveal.js
#-----------------------------: ## -----------------------------

fetch-reveal-js: ${DISTRIBUTION_DIR}/reveal.js/js/reveal.js ## Git fetch reveal.js

${DISTRIBUTION_DIR}/reveal.js/js/reveal.js:
	git clone --branch 3.9.2 --depth 1 https://github.com/hakimel/reveal.js.git ${DISTRIBUTION_DIR}/reveal.js
	@echo "Removing .git dir in cloned reveal.js"
	@rm -rf ${DISTRIBUTION_DIR}/reveal.js/.git

clean-reveal-js: ## Delete git fetched reveal.js
	-@rm -r ${REVEAL_JS_PATHS_TO_BE_CLEANED} 2>/dev/null \
    		&& echo "Deleted ${REVEAL_JS_PATHS_TO_BE_CLEANED}" \
    		|| echo "Nothing to delete for ${@}."


update-reveal-js: clean-reveal-js fetch-reveal-js ## Delete, then git fetch reveal.js

#: ## 
#-----------------------------: ## -----------------------------
#: ## FontAwesome
#-----------------------------: ## -----------------------------

fetch-font-awesome: ${DISTRIBUTION_DIR}/css/font-awesome.min.css ## Download FontAwesome

.ONESHELL:
${DISTRIBUTION_DIR}/css/font-awesome.min.css:
	cd ${DISTRIBUTION_DIR}
	curl https://fontawesome.com/v4.7/assets/font-awesome-4.7.0.zip --output font-awesome-4.7.0.zip
	unzip -o font-awesome-4.7.0.zip */css/font-awesome.min.css */fonts/*
	mv font-awesome-4.7.0/* ./
	rm -r font-awesome-4.7.0 font-awesome-4.7.0.zip

clean-font-awesome: ## Delete downloaded FontAwesome
	-@rm ${FONT_AWESOME_PATHS_TO_BE_CLEANED} 2>/dev/null \
		&& echo "Deleted ${FONT_AWESOME_PATHS_TO_BE_CLEANED}" \
		|| echo "Nothing to delete for ${@}."
