= AsciiDoc Presentation Skeleton

This is a base project for creating an AsciiDoc Presentation
with https://asciidoctor.org/docs/asciidoctor-revealjs/[asciidoctor-revealjs].

== Automatic build and deployment

Fork this project, adapt link:src/index.adoc[] and push the changes.
The slides will be built and served to your gitlab.io pages automatically:

https://{your-gitlab-user}.gitlab.io/{your-project-name}/

TIP: Example link https://tpraxl.gitlab.io/asciidoc-presentation-skeleton/[]

TIP: You might want to remove fork relationship after forking.
To do so, navigate to your project's Settings, expand Advanced
settings, and scroll down to Remove fork relationship.

NOTE: Commits to main will be visible here https://{your-gitlab-user}.gitlab.io/{your-project-name}/
Commits to other branches will be kept for an hour. You can decide to keep the artifacts in your
Gitlab CI / CD section.

== Local development and usage

=== Requirements

Debian based system with:

- unzip
- python3
- curl
- git
- docker (or asciidoctor-revealjs if `MAKE_PRESENTATION_TYPE=native`)

Run `make check-requirements` to verify that the requirements are met.

== Setup

[source, bash]
----
$ make all
----

Run `make` or `make help` to see usage information for `make`.

=== Usage

Update link:src/index.adoc[].

Then, to generate the presentation, run:

[source, bash]
----
$ make presentation
----

View your presentation using a server and point your browser to its url
(default is http://localhost:8080).

[source, bash]
----
$ make serve
----

You can customize the bind address and port like this:

[source, bash]
----
$ make serve SERVE_BIND=192.168.1.1 SERVE_PORT=8000
----
